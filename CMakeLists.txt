cmake_minimum_required(VERSION 2.8)

set(PROJECT_NAME raytrace)
set(PROJECT_VERSION_MAJOR 0)
set(PROJECT_VERSION_MINOR 0)
set(PROJECT_VERSION_PATCH 1)
set(PROJECT_VERSION ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}.${PROJECT_VERSION_PATCH})

project(${PROJECT_NAME})

# Testing support
#include(CTest)

# Packaging
include(InstallRequiredSystemLibraries)
set(CPACK_PACKAGE_CONTACT "Thomas Arcila <thomas.arcila@gmail.com>")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "a simple raytracer")
set(CPACK_PACKAGE_VENDOR "Thomas Arcila <thomas.arcila@gmail.com>")
set(CPACK_PACKAGE_DESCRIPTION_FILE "${CMAKE_CURRENT_SOURCE_DIR}/ReadMe.txt")
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/Copyright.txt")
set(CPACK_PACKAGE_VERSION_MAJOR "${PROJECT_VERSION_MAJOR}")
set(CPACK_PACKAGE_VERSION_MINOR "${PROJECT_VERSION_MINOR}")
set(CPACK_PACKAGE_VERSION_PATCH "${PROJECT_VERSION_PATCH}")
#  debian/rpm
set(CPACK_DEBIAN_PACKAGE_SECTION "devel")
set(CPACK_DEBIAN_PACKAGE_PRIORITY "optional")

set(CPACK_SOURCE_IGNORE_FILES "^${CMAKE_BINARY_DIR}/;\\\\.git;/CVS/;/\\\\.svn/;\\\\.swp$;\\\\.#;/#;.*~;cscope.")
include(CPack)

# Dependencies resolution
include(FeatureSummary)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake")

include_directories(${CMAKE_BINARY_DIR}/include ${CMAKE_BINARY_DIR}/include/eigen2) # for 3rdparties headers

find_package(Boost 1.40 COMPONENTS filesystem system program_options)
set_feature_info(Boost "Free peer-reviewed portable C++ source libraries" "http://www.boost.org")

find_package(Eigen2 2.0.11)
set_feature_info(Eigen2 "C++ template library for linear algebra" "http://eigen.tuxfamily.org")

find_package(PNG 1.2)

add_subdirectory(3rdparties)

include_directories(${EIGEN2_INCLUDE_DIR} ${Boost_INCLUDE_DIR} ${PNG_INCLUDE_DIR})
link_libraries(${PNG_LIBRARIES} ${Boost_LIBRARIES})

set(CMAKE_DEBUG_POSTFIX "d")
add_definitions(-DBOOST_ALL_NO_LIB) # do not use boost autolink feature
if(CMAKE_COMPILER_IS_GNUCC)
  set(CMAKE_C_FLAGS "-Wall -W ${CMAKE_C_CFLAGS}")
endif()
if(CMAKE_COMPILER_IS_GNUCXX)
  set(CMAKE_CXX_FLAGS "-Wall -W ${CMAKE_CXX_CFLAGS}")
endif()

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")

add_subdirectory(src)

add_subdirectory(examples)

