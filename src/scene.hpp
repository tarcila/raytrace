#ifndef SCENE_HPP
#define SCENE_HPP

#include <boost/filesystem.hpp>

#include <list>

class Camera;
class Shape;
class Light;
class Ray;


class Scene
{
public:
    Scene();
    ~Scene();

    static Scene* fromXml(const boost::filesystem::path& filename);

    const Camera* camera() const { return m_camera; }
    void setCamera(Camera* camera) { m_camera = camera; }

    const std::list<const Shape*>& shapes() const { return m_shapes; }
    void setShapes(const std::list<const Shape*> shapes) { m_shapes = shapes; }

    const std::list<const Light*>& lights() const { return m_lights; }
    void setLights(const std::list<const Light*> lights) { m_lights = lights; }

private:
    Camera* m_camera;
    std::list<const Shape*> m_shapes;
    std::list<const Light*> m_lights;
};

#endif // SCENE_HPP
