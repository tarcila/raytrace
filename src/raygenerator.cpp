#include "raygenerator.hpp"
#include "ray.hpp"

#include <Eigen/Geometry>

RayGenerator::RayGenerator(int width, int height, const Camera& camera)
    : m_width(width), m_height(height) {

    m_viewAxis = camera.orientation().normalized();

    m_Xaxis = m_viewAxis.cross(camera.up()).normalized();
    m_Yaxis = camera.up().normalized();

    m_rayPosition = camera.position();

    m_fovY = camera.fieldOfView();
    m_fovX = m_fovY * camera.viewRatio() * static_cast<float>(width) / height;
}

Ray RayGenerator::getRayAt(int i, int j) const
{
    Ray ray;
    ray.setOrigin(m_rayPosition);

    float Xangle = m_fovX * ((float)i / (m_width - 1) - 0.5) ;
    float Yangle = m_fovY * ((float)j / (m_height - 1) - 0.5);

    // counter clock-wise rotations
    Eigen::Quaternionf Xrotation(Eigen::Quaternionf::AngleAxisType(-Xangle, m_Yaxis));
    Eigen::Quaternionf Yrotation(Eigen::Quaternionf::AngleAxisType(-Yangle, m_Xaxis));


    ray.setOrientation(Yrotation * Xrotation* m_viewAxis);
    return ray;
}
