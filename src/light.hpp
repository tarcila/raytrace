#ifndef LIGHT_HPP
#define LIGHT_HPP

#include <Eigen/Core>

#include <list>

class TiXmlElement;
class Shape;

class Light
{
public:
    virtual ~Light() {}
    virtual bool interactWithPoint(const Eigen::Vector3f& point, const std::list<const Shape*>& shapes) const = 0;
};

#endif // LIGHT_HPP
