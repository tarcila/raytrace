#include "raytracer.hpp"
#include "raygenerator.hpp"
#include "shape.hpp"
#include "light.hpp"
#include "scene.hpp"
#include "camera.hpp"
#include "material.hpp"
#include "intersection.hpp"

#include <boost/foreach.hpp>
#include <boost/typeof/typeof.hpp>

#include <iostream>

namespace bg = boost::gil;

bg::rgb32f_pixel_t Raytracer::raytrace(const Ray& ray)
{
    Intersections intersections;

    BOOST_FOREACH(const Shape* shape, m_scene->shapes())
        shape->computeIntersection(ray, intersections);

    bg::rgb32f_pixel_t color(0, 0, 0);

    if (!intersections.empty())
    {
        Intersection nearestIntersection;
        float nearestDistance2 = std::numeric_limits<float>::max();

        BOOST_FOREACH(const Intersection& intersection, intersections)
        {
            Eigen::Vector3f originToCurrentShapeVector(intersection.position() - m_scene->camera()->position());
            float currentShapeDistance2 = originToCurrentShapeVector.dot(originToCurrentShapeVector);
            if (currentShapeDistance2 < nearestDistance2)
            {
                nearestDistance2 = currentShapeDistance2;
                nearestIntersection = intersection;
            }
        }

        std::list<const Light*> lights;
        std::list<const Shape*> allShapesButIntersectedOne = m_scene->shapes();
        allShapesButIntersectedOne.remove(nearestIntersection.shape());

        BOOST_FOREACH(const Light* light, m_scene->lights())
        {
            if (light->interactWithPoint(nearestIntersection.position(), allShapesButIntersectedOne))
                lights.push_back(light);
        }
        color = nearestIntersection.shape()->material()->apply(nearestIntersection, lights);
    }
    return color;
}

void Raytracer::render(bg::rgb32f_image_t& frameBuffer)
{
    BOOST_AUTO(frameBufferView, bg::view(frameBuffer));

    RayGenerator rayGenerator(frameBufferView.width(), frameBufferView.height(), *m_scene->camera());
    for (int y = 0; y < frameBufferView.height(); ++y)
    {
        BOOST_AUTO(frameBufferRowIt, frameBufferView.row_begin(y));
        for (int x = 0; x < frameBufferView.width(); ++x)
            frameBufferRowIt[x] = raytrace(rayGenerator.getRayAt(x, y));
    }
}
