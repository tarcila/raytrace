#!/bin/sh
SCRIPTDIR="$(dirname "${0}")"
export LD_LIBRARY_PATH="${SCRIPTDIR}/../lib:${LD_LIBRARY_PATH}"
export DYLD_LIBRARY_PATH="${SCRIPTDIR}/../lib:${DYLD_LIBRARY_PATH}"
if [ -x "${0}Release" ]; then 
  exec "${0}Release"
else
   if [ -x "${0}Debug" ]; then
    exec "${0}Debug"
  else
    echo "Cannot find any version to run."
  fi
fi
