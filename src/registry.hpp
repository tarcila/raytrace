#ifndef REGISTRY_HPP
#define REGISTRY_HPP

#include <map>
#include <string>
#include <cassert>

class Shape;
class TiXmlElement;

template<class Type>
class Registry
{
public:
    typedef Type* (*CreateFromXmlMethod)(const TiXmlElement*);
    static void registerCreationMethod(const std::string& shapeName, CreateFromXmlMethod createMethod);
    static Type* createFromXml(const TiXmlElement* shapeRoot);

protected:
  typedef std::map<std::string, CreateFromXmlMethod> MapNameToCreateFromXmlMethod;
  static MapNameToCreateFromXmlMethod s_nameToCreateFromXmlMethod;
};

#define REGISTER_XML_CREATION_METHOD_FOR_TYPE_NAME(type, name, creationMethod) \
namespace { \
    int createFromXml##name() { \
        static bool alreadyRegistered = false; \
        assert(!alreadyRegistered); \
        Registry<type>::registerCreationMethod(#name, creationMethod); \
        return 1; \
     } \
     int registerXmlCreationMethodForTypeName = createFromXml##name(); \
}

#endif // REGISTRY_HPP
