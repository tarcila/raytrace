#include "camera.hpp"

#include "tinyxml/tinyxml.h"

#include <stdexcept>

#include <cmath>
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

Camera::Camera(Eigen::Vector3f position, Eigen::Vector3f orientation,
               Eigen::Vector3f up, float fieldOfView, float viewRatio)
{
    m_orientation = orientation;
    m_position = position;
    m_up = up;
    m_fieldOfView = fieldOfView;
    m_viewRatio = viewRatio;
}

Camera* Camera::createFromXml(const TiXmlElement* cameraRoot)
{
    assert(cameraRoot);
    Eigen::Vector3f orientation, position, up;
    double fieldOfView, viewRatio;

    bool hasOrientation = false, hasPosition = false, hasUp = false;
    bool hasFieldOfViewRatio = false;


    const char* tmp = cameraRoot->Attribute("id");
    std::string id;
    if (tmp)
        id = tmp;

    double x, y, z;
    double fov, ratio;

    for (const TiXmlElement* element = cameraRoot->FirstChildElement(); element; element = element->NextSiblingElement())
    {
        std::string elementName(element->Value());
        if (elementName == "position" && !hasPosition)
        {
            if ((hasPosition = (element->Attribute("x", &x) && element->Attribute("y", &y) && element->Attribute("z", &z))))
                position = Eigen::Vector3f(x, y, z);
        }
        else if (elementName == "orientation" && !hasOrientation)
        {
            if ((hasOrientation = (element->Attribute("x", &x) && element->Attribute("y", &y) && element->Attribute("z", &z))))
                orientation = Eigen::Vector3f(x, y, z);
        }
        else if (elementName == "up" && !hasUp)
        {
            if ((hasUp = (element->Attribute("x", &x) && element->Attribute("y", &y) && element->Attribute("z", &z))))
                up = Eigen::Vector3f(x, y, z);
        }
        else if (elementName == "fov" && !hasFieldOfViewRatio)
        {
            if ((hasFieldOfViewRatio = element->Attribute("vertical", &fov) && element->Attribute("aspect", &ratio)))
            {
                fieldOfView = fov * M_PI / 180.;
                viewRatio = ratio;
            }
        }
        else
            std::cout << "Unknown/redefined element " << elementName << " for a camera." << std::endl;
    }

    if (!hasPosition)
        throw std::runtime_error(std::string("camera[") + id + "] has no valid position definition.");
    if (!hasOrientation)
        throw std::runtime_error(std::string("camera[") + id + "] has no valid orientation definition.");
    if (!hasUp)
        throw std::runtime_error(std::string("camera[") + id + "] has no valid up definition.");

    if (!hasFieldOfViewRatio)
        throw std::runtime_error(std::string("camera[") + id + "] has no valid field of view definition.");

    return new Camera(position, orientation, up, fieldOfView, viewRatio);
}
