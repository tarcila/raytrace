#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "ray.hpp"
#include "intersection.hpp"

#include <list>

class Material;

class Shape
{
public:
    virtual ~Shape() {}

    const Material* material() const { return m_material; }
    void setMaterial(const Material* material) { m_material = material; }
    virtual bool computeIntersection(const Ray& ray, Intersections& intersections) const = 0;

protected:
    const Material* m_material;

    Shape(const Material* material)
    : m_material(material) {
    }
};

#endif // SHAPE_HPP
