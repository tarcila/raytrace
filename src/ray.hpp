#ifndef RAY_HPP
#define RAY_HPP

#include <Eigen/Core>

class Ray
{
public:
    Ray()
        : m_origin(0, 0, 0), m_orientation(0, 0, 1) {
    }

    Ray(const Eigen::Vector3f& origin, const Eigen::Vector3f& orientation)
        : m_origin(origin), m_orientation(orientation) {
    }

    const Eigen::Vector3f& origin() const { return m_origin; }
    void setOrigin(const Eigen::Vector3f& origin) { m_origin = origin; }
    const Eigen::Vector3f& orientation() const { return m_orientation; }
    void setOrientation(const Eigen::Vector3f& orientation) { m_orientation = orientation; }

private:
    Eigen::Vector3f m_origin;
    Eigen::Vector3f m_orientation;
};

#endif // RAY_HPP
