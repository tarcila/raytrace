#include "phong.hpp"

#include "../lights/pointlight.hpp"
#include "../intersection.hpp"

#include "../tinyxml/tinyxml.h"

#include <boost/foreach.hpp>
#include <stdexcept>

namespace bg = boost::gil;

bg::rgb32f_pixel_t Phong::apply(const Intersection& intersection, const std::list<const Light*>& lights) const
{
    Eigen::Vector3f color = m_ambientColor;

    BOOST_FOREACH(const Light* light, lights)
    {
        if (const PointLight* pointLight = dynamic_cast<const PointLight*>(light))
        {
            Eigen::Vector3f pointToLightVector = pointLight->position() - intersection.position();
            // assume white ambient color = m_ambientColor.dot(pointLight->ambientColor());
	    color += std::max(0.f, pointToLightVector.normalized().dot(intersection.normal().normalized())) * m_diffuseColor /* * pointLight->diffuseColor() */;

        }
    }

    return bg::rgb32f_pixel_t(color.x(), color.y(), color.z());
}

Material* Phong::createFromXml(const TiXmlElement* materialRoot)
{
    assert(materialRoot);
    const char* tmp = materialRoot->Attribute("id");
    std::string id;
    if (tmp)
        id = tmp;

    double r, g, b;
    Eigen::Vector3f ambientColor, diffuseColor, specularColor;
    bool hasAmbientColor = false, hasDiffuseColor = false, hasSpecularColor = false;
    double specularPower = 1.;
    bool hasSpecularPower = false;

    for (const TiXmlElement* element = materialRoot->FirstChildElement(); element; element = element->NextSiblingElement())
    {
        std::string elementName(element->Value());
        if (elementName == "ambient" && !hasAmbientColor) {
            if ((hasAmbientColor = (element->Attribute("r", &r) && element->Attribute("g", &g) && element->Attribute("b", &b))))
                ambientColor = Eigen::Vector3f(r, g, b);
        } else if (elementName == "diffuse" && !hasDiffuseColor) {
            if ((hasDiffuseColor = (element->Attribute("r", &r) && element->Attribute("g", &g) && element->Attribute("b", &b))))
                diffuseColor = Eigen::Vector3f(r, g, b);
        } else if (elementName == "specular" && !hasSpecularColor) {
            if ((hasSpecularColor = (element->Attribute("r", &r) && element->Attribute("g", &g) && element->Attribute("b", &b))))
                specularColor = Eigen::Vector3f(r, g, b);
            element->Attribute("power", &specularPower);
        } else
            std::cout << "Unknown/redefined element " << elementName << " for a light." << std::endl;
    }

    if (!hasAmbientColor)
        throw std::runtime_error(std::string("material[") + id + "] has no valid ambient color definition.");

    if (!hasDiffuseColor)
        throw std::runtime_error(std::string("material[") + id + "] has no valid diffuse color definition.");

    if (!hasSpecularColor)
        throw std::runtime_error(std::string("material[") + id + "] has no valid specular color definition.");

    std::cout << "creating material[" << id << "]" << std::endl;
    return new Phong(ambientColor, diffuseColor, specularColor, specularPower);
}
