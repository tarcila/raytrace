#ifndef PHONG_HPP
#define PHONG_HPP

#include "../material.hpp"

#include <Eigen/Core>

class TiXmlElement;

class Phong : public Material
{
public:
    Phong(const Eigen::Vector3f& ambientColor, const Eigen::Vector3f& diffuseColor,
	  const Eigen::Vector3f& specularColor, float brillance)
    : m_ambientColor(ambientColor), m_diffuseColor(diffuseColor),
    m_specularColor(specularColor), m_brillance(brillance) {
    }

    const Eigen::Vector3f& ambientColor() const { return m_ambientColor; }
    void setAmbientColor(const Eigen::Vector3f& ambientColor) { m_ambientColor = ambientColor; }

    const Eigen::Vector3f& diffuseColor() const { return m_diffuseColor; }
    void setDiffuseColor(const Eigen::Vector3f& diffuseColor) { m_diffuseColor = diffuseColor; }

    const Eigen::Vector3f& specularColor() const { return m_specularColor; }
    void setSpecularColor(const Eigen::Vector3f& specularColor) { m_specularColor = specularColor; }

    float brillance() const { return m_brillance; }
    void setBrillance(float brillance) { m_brillance = brillance; }

    virtual boost::gil::rgb32f_pixel_t apply(const Intersection& intersection, const std::list<const Light*>& lights) const;

    static Material* createFromXml(const TiXmlElement* materialRoot);
private:
    Eigen::Vector3f m_ambientColor;
    Eigen::Vector3f m_diffuseColor;
    Eigen::Vector3f m_specularColor;
    float m_brillance;
};

#endif // PHONG_HPP
