#ifndef POINTLIGHT_HPP
#define POINTLIGHT_HPP

#include "../light.hpp"

class PointLight : public Light
{
public:
     PointLight(const Eigen::Vector3f& position)
    : m_position(position) {
    }

    const Eigen::Vector3f& position() const { return m_position; }
    void setPosition(const Eigen::Vector3f& position) { m_position = position; }

    virtual bool interactWithPoint(const Eigen::Vector3f& point, const std::list<const Shape*>& shapes) const;

    static Light* createFromXml(const TiXmlElement* lightRoot);
private:
    Eigen::Vector3f m_position;
};

#endif // POINTLIGHT_HPP
