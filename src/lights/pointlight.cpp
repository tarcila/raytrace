#include "pointlight.hpp"
#include "../shape.hpp"

#include <Eigen/Core>

#include "../tinyxml/tinyxml.h"

#include <boost/foreach.hpp>
#include <stdexcept>

Light* PointLight::createFromXml(const TiXmlElement* lightRoot)
{
    assert(lightRoot);
    const char* tmp = lightRoot->Attribute("id");
    std::string id;
    if (tmp)
        id = tmp;

    double x, y, z;
    bool hasPosition = false;

    for (const TiXmlElement* element = lightRoot->FirstChildElement(); element; element = element->NextSiblingElement())
    {
        std::string elementName(element->Value());
        if (elementName == "position" && !hasPosition)
            hasPosition = (element->Attribute("x", &x) && element->Attribute("y", &y) && element->Attribute("z", &z));
        else
            std::cout << "Unknown/redefined element " << elementName << " for a light." << std::endl;
    }

    if (!hasPosition)
        throw std::runtime_error(std::string("light[") + id + "] has no valid center definition.");

    std::cout << "creating light[" << id << "]" << std::endl;
    return new PointLight(Eigen::Vector3f(x, y, z));
}

bool PointLight::interactWithPoint(const Eigen::Vector3f& point, const std::list<const Shape*>& shapes) const
{
    Intersections intersections;
    Eigen::Vector3f lightIntersectionVector(point - m_position);
    float lightIntersectionDistance2 = lightIntersectionVector.dot(lightIntersectionVector);

    Ray lightIntersectionRay(m_position, lightIntersectionVector.normalized());
    BOOST_FOREACH(const Shape* shape, shapes)
        shape->computeIntersection(lightIntersectionRay, intersections);


    int validIntersections = 0;
    BOOST_FOREACH(const Intersection& intersection, intersections)
    {
        Eigen::Vector3f lightCurrentIntersectionVector(intersection.position() - m_position);
        if (lightCurrentIntersectionVector.dot(lightCurrentIntersectionVector) < lightIntersectionDistance2)
            validIntersections++;
    }

    return validIntersections == 0;
}
