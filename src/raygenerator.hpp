#ifndef RAYGENERATOR_HPP
#define RAYGENERATOR_HPP

#include "camera.hpp"
#include "ray.hpp"

#include <Eigen/Core>

class RayGenerator
{
public:
    RayGenerator(int width, int height, const Camera& camera);

    Ray getRayAt(int i, int j) const;

private:
    int m_width;
    int m_height;

    Eigen::Vector3f m_viewAxis;

    Eigen::Vector3f m_Xaxis;
    Eigen::Vector3f m_Yaxis;

    float m_fovX;
    float m_fovY;

    Eigen::Vector3f m_rayPosition;
};

#endif // RAYGENERATOR_HPP
