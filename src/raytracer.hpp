#ifndef RAYTRACER_HPP
#define RAYTRACER_HPP

#include "ray.hpp"

#include <boost/gil/gil_all.hpp>

class Scene;

class Raytracer
{
public:
    Raytracer(const Scene* scene)
    : m_scene(scene) {
    }

    void render(boost::gil::rgb32f_image_t& frameBuffer);

private:
    const Scene* m_scene;
    boost::gil::rgb32f_pixel_t raytrace(const Ray& ray);
};

#endif // RAYTRACER_HPP
