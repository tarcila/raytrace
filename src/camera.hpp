#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <eigen2/Eigen/Core>

class TiXmlElement;

class Camera
{
public:
    Camera(Eigen::Vector3f position, Eigen::Vector3f orientation,
           Eigen::Vector3f up, float fieldOfView, float viewRatio = 1.f);

    static Camera* createFromXml(const TiXmlElement* cameraRoot);

    const Eigen::Vector3f& position() const { return m_position; }
    void setPosition(const Eigen::Vector3f& position) { m_position = position; }

    const Eigen::Vector3f& orientation() const { return m_orientation; }
    void setOrientation(const Eigen::Vector3f& orientation) { m_orientation = orientation; }

    const Eigen::Vector3f& up() const { return m_up; }
    void setUp(const Eigen::Vector3f& up) { m_up = up; }

    float fieldOfView() const { return m_fieldOfView; }
    void setFieldOfView(float fieldOfView) { m_fieldOfView = fieldOfView; }

    float viewRatio() const { return m_viewRatio; }
    void setViewRatio(float viewRatio) { m_viewRatio = viewRatio; }

private:
    Eigen::Vector3f m_position;
    Eigen::Vector3f m_orientation;
    Eigen::Vector3f m_up;
    float m_fieldOfView;
    float m_viewRatio;
};

#endif // CAMERA_HPP
