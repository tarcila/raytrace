#include "scene.hpp"
#include "raytracer.hpp"

#include "shapes/sphere.hpp"
#include "shaperegistry.hpp"
#include "lights/pointlight.hpp"
#include "lightregistry.hpp"
#include "materials/phong.hpp"
#include "materialregistry.hpp"

#include <png.h>
#ifndef png_infopp_NULL
#define png_infopp_NULL (png_infopp)NULL
#define int_p_NULL (int*)NULL
#endif

#include <boost/gil/gil_all.hpp>
#include <boost/gil/extension/io/png_io.hpp>
#include <boost/typeof/typeof.hpp>
#include <boost/foreach.hpp>
#include <boost/format.hpp>

namespace bg = boost::gil;

#define WIDTH 1680
#define HEIGHT 1050

int main(int /*argc*/, char** /*argv*/)
{
    ShapeRegistry::registerCreationMethod("sphere", &Sphere::createFromXml);
    LightRegistry::registerCreationMethod("point-light", &PointLight::createFromXml);
    MaterialRegistry::registerCreationMethod("phong", &Phong::createFromXml);

    Scene* scene = Scene::fromXml("../examples/scenes/scene.xml");
    Raytracer raytracer(scene);

    bg::rgb32f_image_t image(WIDTH, HEIGHT);
    raytracer.render(image);

    float min = std::numeric_limits<float>::max();
    float max = std::numeric_limits<float>::min();

    BOOST_AUTO(imageView, bg::const_view(image));
    BOOST_AUTO(imageViewIt, imageView.begin());
    for(imageViewIt = imageView.begin();
        imageViewIt != imageView.end(); ++imageViewIt)
    {
        bg::rgb32_pixel_t pixel = *imageViewIt;

        if (pixel[0] < min)
            min = pixel[0];
        if (pixel[1] < min)
            min = pixel[1];
        if (pixel[2] < min)
            min = pixel[2];
        if (pixel[0] > max)
            max = pixel[0];
        if (pixel[1] > max)
            max = pixel[1];
        if (pixel[2] > max)
            max = pixel[2];
    }
    float scale = 255.f / (max - min);

    bg::rgb8_image_t image256(image.width(), image.height());
    BOOST_AUTO(image256View, view(image256));

    BOOST_AUTO(image256ViewIt, image256View.begin());
    for(imageViewIt = imageView.begin(), image256ViewIt = image256View.begin();
        imageViewIt != imageView.end(); ++imageViewIt, ++image256ViewIt)
    {
        bg::rgb32f_pixel_t pixel = *imageViewIt;
        bg::rgb8_pixel_t outputPixel;
        outputPixel[0] = (pixel[0] - min) * scale;
        outputPixel[1] = (pixel[1] - min) * scale;
        outputPixel[2] = (pixel[2] - min) * scale;
        *image256ViewIt = outputPixel;
    }

    bg::png_write_view((boost::format("output-%1%x%2%.png") % WIDTH % HEIGHT).str().c_str(), image256View);
    return 0;
}
