#ifndef INTERSECTION_HPP
#define INTERSECTION_HPP

#include <Eigen/Core>

#include <list>

class Shape;

class Intersection
{
public:
    Intersection()
            : m_position(0, 0, 0), m_normal(0, 0, 1), m_shape(0) {
    }
    Intersection(const Eigen::Vector3f& position, const Eigen::Vector3f& normal, const Shape* shape)
            : m_position(position), m_normal(normal), m_shape(shape) {
    }

    const Eigen::Vector3f& position() const {
        return m_position;
    }
    void setPosition(const Eigen::Vector3f& position) {
        m_position = position;
    }
    const Eigen::Vector3f& normal() const {
        return m_normal;
    }
    void setNormal(const Eigen::Vector3f& normal) {
        m_normal = normal;
    }
    const Shape* shape() const {
        return m_shape;
    }
    void setShape(const Shape* shape) {
        m_shape = shape;
    }

private:
    Eigen::Vector3f m_position, m_normal;
    const Shape* m_shape;
};

typedef std::list<Intersection> Intersections;

#endif // INTERSECTION_HPP
