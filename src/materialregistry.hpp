#ifndef MATERIALREGISTRY_HPP
#define MATERIALREGISTRY_HPP

#include "material.hpp"
#include "registry.hpp"

class MaterialRegistry : public Registry<Material>
{
public:
    static Material* getFromName(const std::string& name);
    static void registerInstance(const std::string& name, Material* material);

private:
    typedef std::map<std::string, Material*> MapNameToInstance;
    static MapNameToInstance s_nameToInstance;;
};
#endif // MATERIALREGISTRY_HPP
