#ifndef SHAPEREGISTRY_HPP
#define SHAPEREGISTRY_HPP

#include "shape.hpp"
#include "registry.hpp"

typedef Registry<Shape> ShapeRegistry;

#endif // SHAPEREGISTRY_HPP
