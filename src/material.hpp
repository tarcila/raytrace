#ifndef MATERIAL_HPP
#define MATERIAL_HPP

#include <boost/gil/gil_all.hpp>

#include <list>

class Intersection;
class Light;

class Material
{
public:
    virtual ~Material() {}
    virtual boost::gil::rgb32f_pixel_t apply(const Intersection& intersection, const std::list<const Light*>& light) const = 0;
};

#endif // MATERIAL_HPP
