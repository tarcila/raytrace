#include "registry.hpp"

#include "tinyxml/tinyxml.h"
#include <stdexcept>

template<class Type>
typename Registry<Type>::MapNameToCreateFromXmlMethod Registry<Type>::s_nameToCreateFromXmlMethod;

template<class Type>
void Registry<Type>::registerCreationMethod(const std::string& name, typename Registry<Type>::CreateFromXmlMethod createMethod)
{
    typename MapNameToCreateFromXmlMethod::const_iterator existingItem = s_nameToCreateFromXmlMethod.find(name);
    if (existingItem == s_nameToCreateFromXmlMethod.end())
        s_nameToCreateFromXmlMethod[name] = createMethod;
    else
        throw std::runtime_error(name + " is an already registered shape name.");
}

template<class Type>
Type* Registry<Type>::createFromXml(const TiXmlElement* root)
{
    assert(root);

    std::string name = root->Value();

    typename MapNameToCreateFromXmlMethod::const_iterator existingItem = s_nameToCreateFromXmlMethod.find(name);
    if (existingItem == s_nameToCreateFromXmlMethod.end())
        throw std::runtime_error(name + " is not a valid name.");
    else
        return s_nameToCreateFromXmlMethod[name](root);
}

#define INSTANCIATE_REGISTRY(type) \
template void Registry<type>::registerCreationMethod(const std::string&, Registry<type>::CreateFromXmlMethod); \
template type* Registry<type>::createFromXml(const TiXmlElement*)
