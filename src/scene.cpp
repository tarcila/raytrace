#include "scene.hpp"

#include "camera.hpp"
#include "shape.hpp"
#include "shaperegistry.hpp"
#include "lightregistry.hpp"
#include "materialregistry.hpp"
#include "ray.hpp"
#include "raygenerator.hpp"

#include <limits>
#include <cmath>
#include <stdexcept>

#include "tinyxml/tinyxml.h"

namespace bf = boost::filesystem;

Scene::Scene()
    : m_camera(0)
{
}

Scene* Scene::fromXml(const bf::path& filename)
{
    TiXmlDocument sceneXml(filename.string().c_str());
    if (!sceneXml.LoadFile())
        throw std::runtime_error(filename.string() + " : unable to load scene file.");

    TiXmlElement* element = sceneXml.FirstChildElement();
    if (!element || std::string(element->Value()) != "scene")
        throw std::runtime_error(filename.string() + " : is not a valid xml scene file.");

    std::list<const Shape*> shapes;
    std::list<const Light*> lights;
    std::list<const Material*> materials;
    Camera* camera = 0;

    // iterate through scene description
    for (element = element->FirstChildElement(); element; element = element->NextSiblingElement())
    {
        std::string elementName = element->Value();
        if (elementName == "camera")
            camera = Camera::createFromXml(element);
        else if (elementName == "shapes")
        {
            for (TiXmlElement* shapeElement = element->FirstChildElement(); shapeElement; shapeElement = shapeElement->NextSiblingElement())
            {
                Shape* shape = ShapeRegistry::createFromXml(shapeElement);
                if (!shape)
                    std::cout << "Unknown shape type " << shapeElement->Value() << std::endl;
                else
                    shapes.push_back(shape);
            }
        }
        else if (elementName == "lights")
        {
            for (TiXmlElement* lightElement = element->FirstChildElement(); lightElement; lightElement = lightElement->NextSiblingElement())
            {
                Light* light = LightRegistry::createFromXml(lightElement);
                if (!light)
                    std::cout << "Unknown shape type " << lightElement->Value() << std::endl;
                else
                    lights.push_back(light);
            }
        }
        else if (elementName == "materials")
        {
            for (TiXmlElement* materialElement = element->FirstChildElement(); materialElement; materialElement = materialElement->NextSiblingElement())
            {
                Material* material = MaterialRegistry::createFromXml(materialElement);
                if (!material)
                    std::cout << "Unknown shape type " << materialElement->Value() << std::endl;
                else
                {
                    MaterialRegistry::registerInstance(materialElement->Attribute("id"), material);
                    materials.push_back(material);
                }
            }
        }
        else
            std::cout << "Skipping unknown description " << elementName << std::endl;
    }

    if (!camera || shapes.empty() || lights.empty())
        throw std::runtime_error("given scene has no camera, primitive or light defined.");

    Scene* scene = new Scene;
    scene->setShapes(shapes);
    scene->setLights(lights);
    scene->setCamera(camera);
    return scene;
}

Scene::~Scene()
{
    delete m_camera;
}
