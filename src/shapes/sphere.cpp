#include "sphere.hpp"
#include "../shaperegistry.hpp"
#include "../materialregistry.hpp"

#include "../tinyxml/tinyxml.h"

#include <stdexcept>
#include <cmath>

bool Sphere::computeIntersection(const Ray& ray, Intersections& intersections) const
{
    Eigen::Vector3f l = m_center - ray.origin();
    float s = l.dot(ray.orientation());
    float l2 = l.dot(l);
    float r2 = m_radius * m_radius;

    if (s < 0 && l2 > r2)
        return false;

    float m2 = l2 - s*s;
    if (m2 > r2)
        return false;

    float q = std::sqrt(r2-m2);
    float t;
    if (l2 > r2)
        t = s - q;
    else
        t = s + q;

    Eigen::Vector3f position = ray.origin() + t * ray.orientation();
    Eigen::Vector3f normal = position - m_center;

    intersections.push_back(Intersection(position, normal, this));
    return true;
}

Shape* Sphere::createFromXml(const TiXmlElement* shapeRoot)
{
    assert(shapeRoot);
    const char* tmp = shapeRoot->Attribute("id");
    std::string id;
    if (tmp)
        id = tmp;

    tmp = shapeRoot->Attribute("material");
    std::string material;
    if (tmp)
        material = tmp;

    double cx, cy, cz;
    bool hasCenter = false;
    double radius;
    bool hasRadius = false;

    for (const TiXmlElement* element = shapeRoot->FirstChildElement(); element; element = element->NextSiblingElement())
    {
        std::string elementName(element->Value());
	if (elementName == "center" && !hasCenter)
	    hasCenter = (element->Attribute("x", &cx) && element->Attribute("y", &cy) && element->Attribute("z", &cz));
	else if (elementName == "radius" && !hasRadius)
	    hasRadius = element->Attribute("value", &radius);
	else
	    std::cout << "Unknown/redefined element " << elementName << " for a sphere shape." << std::endl;
    }

    if (!hasCenter)
        throw std::runtime_error(std::string("sphere[") + id + "] has no valid center definition.");

    if (!hasRadius)
        throw std::runtime_error(std::string("sphere[") + id + "] has no valid radius definition.");

    std::cout << "creating sphere[" << id << "]" << std::endl;
    return new Sphere(Eigen::Vector3f(cx, cy, cz), radius, MaterialRegistry::getFromName(material));
}
