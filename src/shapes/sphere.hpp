#ifndef SPHERE_HPP
#define SPHERE_HPP

#include "../shape.hpp"
#include "../shaperegistry.hpp"

#include <Eigen/Core>

class Sphere : public Shape
{
public:
    Sphere(const Eigen::Vector3f& center, float radius, const Material* material)
        : Shape(material), m_center(center), m_radius(radius) {
    }

    virtual bool computeIntersection(const Ray& ray, Intersections& intersections) const;

    static Shape* createFromXml(const TiXmlElement* shapeRoot);

private:
    Eigen::Vector3f m_center;
    float m_radius;
};

#endif // SPHERE_HPP
