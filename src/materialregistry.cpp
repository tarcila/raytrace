#include "materialregistry.hpp"

#include "registry.inc.cpp"

INSTANCIATE_REGISTRY(Material);

MaterialRegistry::MapNameToInstance MaterialRegistry::s_nameToInstance;

Material* MaterialRegistry::getFromName(const std::string& name)
{
    MapNameToInstance::iterator instanceIt = s_nameToInstance.find(name);
    assert(instanceIt != s_nameToInstance.end()); // replace with default materialregistry
    return instanceIt->second;
}

void MaterialRegistry::registerInstance(const std::string& name, Material* material)
{
  assert(s_nameToInstance.find(name) == s_nameToInstance.end());
  s_nameToInstance[name] = material;
}
