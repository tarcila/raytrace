#ifndef LIGHTREGISTRY_HPP
#define LIGHTREGISTRY_HPP

#include "light.hpp"
#include "registry.hpp"

typedef Registry<Light> LightRegistry;

#endif // LIGHTREGISTRY_HPP
